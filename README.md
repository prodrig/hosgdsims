# hosgdSims

* This simulations are related to [SC-1 Short Courses @ ICASSP'23](https://2023.ieeeicassp.org/education-short-courses/).
* The associated lectures may be found [here](https://hands-on-sgd.readthedocs.io/)

### Usage

Read about the intended use [here](https://hosgdSims.readthedocs.io/)


### Requirements

Python 3.x


### Legal

hosgdSims is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License (version 3).
