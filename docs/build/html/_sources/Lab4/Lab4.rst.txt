
Lecture 4
=========


.. collapse:: Theory

  To be updated

.. raw:: html

    <hr style="border:2px solid gray"> </hr>


These hands-on exercises focus on augmenting the TensorFlow (TF) library by implementing a given SGD variant, to then compare it versus the corresponding version shipped with TF, for the multiclass classification problem.



.. toctree::
   :maxdepth: 1
   :caption: Simulations

   TF.md
