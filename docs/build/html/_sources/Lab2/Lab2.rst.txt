
Lecture 2
=========

.. collapse:: Theory

  Lecture slides:

  * Adaptive step-size: `Here <https://drive.google.com/file/d/1mIwyL5mdkBlix_OLzHcZHSq_zgqTeN7W/view?usp=sharing>`_
  * Accelerated GD: `Here <https://drive.google.com/file/d/1SYmOWzqIwNOzLAU2J9HwUJV8Qfv5YAWk/view?usp=sharing>`_


.. raw:: html

    <hr style="border:2px solid gray"> </hr>


These hands-on exercesises focus on first augmenting the Gradient Descent (GD) algorithm by
(i) implementing adpative step-size methods, as well as (ii) accelerated GD (AGD) methods,
such Momentum (a.k.a. Polyak's heavy ball :cite:ps:`polyak-1964-some`) and Nesterov :cite:ps:`nesterov-1983-method` acceleration.

The second part focuses on augmenting the Stochastic GD (SGD) algorithm by
implementing adaptive SGD variants, such AdaGrad :cite:ps:`duchi-2011-adaptive`, RMSprop :cite:ps:`hinton-2012-neural`,
ADAM :cite:ps:`kingma-2015-adam`, etc.
Such implementations will be tested on the multiclass classification problem (MNIST and CIFAR10 datasets)
and compared to te previously developed SGD.

Contents:

.. toctree::
   :maxdepth: 1
   :caption: Simulations

   AGD.md
   SGDvar.md
