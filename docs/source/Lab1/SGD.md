
<!-- .dDown[open] > summary {
  box-shadow: 3px 3px 4px black;
  cursor: pointer;
  list-style: none;}

  DarkOliveGreen
  DarkRed
  #283747;  similar (but darker) to
  DarkSlateGray;
-->


<style type="text/css">
<!--
 .tab1 { margin-left: 1em;}
 .tab2 { margin-left: 2em;}
 .dDown[open] > summary {
  color: DarkSlateGray;}
 .dDown > summary {
  color: DarkSlateGray;}
-->
</style>


(theorySGD)=
# SGD


Consider cost functional


\begin{eqnarray*}
F(\mathbf{u}) & =  & \sum_{n=0}^{N-1} f_n(\mathbf{u})  \qquad (2)
\end{eqnarray*}

````{tabbed} where


* $f(\cdot) : \mathbb{R}^M \mapsto \mathbb{R}$ is differentiable.
* The opposite of the gradient, i.e. $- \nabla F(\mathbf{u})$ always points towards the minimum $\mathbf{u}^*$: <br>
  \begin{eqnarray*} \forall\epsilon> 0 & &
  \inf_{\| \mathbf{u}-\mathbf{u}^* \|_2^2} \langle \mathbf{u}-\mathbf{u}^*, \nabla F(\mathbf{u}) \rangle  > 0\end{eqnarray*}

````

````{tabbed} E.g. Softmax loss

* Softmax loss function (for a full explanation see [Lecture 3b's introduction](https://drive.google.com/file/d/1Kt1wPgYzIvMYtM19ft-XrSqrZ-Td43jb/view?usp=sharing)) <br>
  \begin{eqnarray*} F(\mathbf{W}) & = & \sum_{n=0}^N \langle \mathbf{x}_n, \mathbf{w}_{_{\mathbf{\cal{L}}_n}} \rangle +
                      \sum_{n=0}^N \log\left( \sum_{l=0}^{L-1}
                      \mbox{e}^{ -\langle \mathbf{x}_n, \mathbf{w}_{l} \rangle }\right) \end{eqnarray*}

  where
  * $\mathbf{W} \in \mathbb{R}^{L \times S }$
  * $L$ represent the number of classes
  * $S$ represent the number of 'features'
  * $\mathbf{X} \in \mathbb{R}^{N \times S }$ represents the training dataset,

  and
  * $\mathbf{w}_l$ represents the l-th column of matrix $\mathbf{W}$.
  * $\mathbf{x}_n$ represents one training data.

````

````{tabbed} Useful routines

  \begin{eqnarray*} F(\mathbf{W}) & = & \frac{1}{N}\left( \sum_{n=0}^N \langle \mathbf{x}_n, \mathbf{w}_{_{\mathbf{\cal{L}}_n}} \rangle +
                      \sum_{n=0}^N \log\left( \sum_{l=0}^{L-1}
                      \mbox{e}^{ -\langle \mathbf{x}_n, \mathbf{w}_{l} \rangle }\right) \right) +
                      \lambda \| \mathbf{W} \|_F^2 \end{eqnarray*}

```python
def softmax_L2_cost(X, Y, W, lmbd):

    fCost = np.sum(np.log(np.sum(np.exp(-X.transpose().dot(W)), axis=1)))
    fCost += np.sum( X*W[:,Y] )

    fCost /= float(X.shape[1])

    if lmbd > 0:
       fCost += lmbd*(sum( np.power(W.ravel(),2) ))/float(X.shape[1])

    return fCost

```

  \begin{eqnarray*} g(\mathbf{W}, {\cal{I}}_k) & = & \frac{1}{\#{\cal{I}}_k} \sum_{n \in {\cal{I}}_k} \nabla f_n(\mathbf{W}) \\
                    & & \\
                    f_n(\mathbf{W}) & = & \langle \mathbf{x}_n, \mathbf{w}_{_{\mathbf{\cal{L}}_n}} \rangle +
                    \log\left( \sum_{l=0}^{L-1} \mbox{e}^{ -\langle \mathbf{x}_n, \mathbf{w}_{l} \rangle }\right)
                    + \lambda \| \mathbf{W} \|_F^2
  \end{eqnarray*}

```python
def softmax_L2_grad(X, Y, W, n, lmbd):

    z = X[:,np.ix_(n)].squeeze(1)      # select batch elements

    g = -z.dot( softmax(-z.transpose().dot(W),axis=1) )

    for k in range(len(n)):
        g[:,Y[n[k]] ] += z[:,k]     # Y is assumed to be ravel()

    g /= float(len(n))

    if lmbd > 0:
       g += lmbd*W

    return g
```

````

````{tabbed} Loading MNIST/CIFAR10

```{literalinclude} ../../../hosgdSims/HoSGDlabSGD.py
:language: python
:pyobject: stdPreproc
```

```python
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.datasets import mnist
from tensorflow.keras.datasets import cifar10

(X, Y), (Xtest, Ytest) = mnist.load_data()
#(X, Y), (Xtest, Ytest) = cifar10.load_data()

# standard pre-processing
X     = stdPreproc(X)
Xtest = stdPreproc(Xtest)


# if needed --> data vectorization
X     = np.transpose( X.reshape(X.shape[0], np.array(X.shape[1::]).prod()) )
Xtest = np.transpose( Xtest.reshape(Xtest.shape[0], np.array(Xtest.shape[1::]).prod()) )
Y     = Y.ravel()
Ytest = Ytest.ravel()

```

````

<u>Problem</u>: write you own {hoverxref}`SGD<algoSGD>` routine; as a test function use the Softmax function (as described in "Softmax loss" tab of the above panel); use the MNIST / CIFAR10 dataset and solve the multiclass classification problem using your SGD routine.


<details><summary><strong>Proposed solution</strong></summary>

<div style="line-height:50%;"> <br> </div>

(routineSGD)=
Among other pausible alternatives, the SGD algorithm may be written as

````{tabbed} Algorithm

```{literalinclude} ../../../hosgdSims/HoSGDlabSGD.py
:language: python
:pyobject: hosgdSGD
```

````

````{tabbed} Parameters

```{literalinclude} ../../../hosgdSims/HoSGDlabSGD.py
:language: python
:pyobject: hosgdSGD
:lines: 1
```

<details><summary><strong>OptProb</strong></summary>

~~~{tabbed} Description
* <code class="language-python">class</code> that encapsultes a particular optimization problem.
* Given the function $F(\cdot) : \mathbb{R}^N \mapsto \mathbb{R}$, this <code class="language-python">class</code> should implement
    * <code class="language-python">costFun</code>: computes the cost functional.
    * <code class="language-python">gradFun</code>: computes the gradient, i.e. $ \nabla F(\cdot) $.
    * <code class="language-python">randSol</code>: generates a random initial solution
    * <code class="language-python">computeStats</code>: gathers useful statistics (e.g. cost functional, current learning rate value, etc.).
    * <code class="language-python">computeSuccess</code>: if a test dataset is given, then compute the succes rate.
~~~

~~~{tabbed} E.g. Softmax.
```{literalinclude} ../../../hosgdSims/HoSGDlabSGD.py
:language: python
:pyobject: hosgdFunSoftMax
```
~~~

</details>

<div style="line-height:50%;"> <br> </div>

<details><summary><strong>nEpoch</strong></summary>
  <div style="line-height:50%;"> <br> </div>
  Total number of epochs.
</details>

<div style="line-height:50%;"> <br> </div>

<details><summary><strong>blkSize</strong></summary>
  <div style="line-height:50%;"> <br> </div>
  Block size.
</details>

<div style="line-height:50%;"> <br> </div>

<details><summary><strong>alpha0</strong></summary>
  <div style="line-height:50%;"> <br> </div>
  Initial learning rate.
</details>


<div style="line-height:50%;"> <br> </div>

(hyperFunSGD)=
<details><summary><strong>hyperFun</strong></summary>

~~~{tabbed} Description
* <code class="language-python">class</code>  that encapsultes any functionality related to SGD's hyper-parameters.
* Up to this point, this <code class="language-python">class</code> would only focus on replicating the initial
  learning rate $\alpha_0$ or in implementing  a Step-decay policy.
~~~

~~~{tabbed} E.g.
```{literalinclude} ../../../hosgdSims/HoSGDlabSGD.py
:language: python
:pyobject: hosgdHyperFunSGD
```
~~~

~~~{tabbed} class `lrSGD` (learning rate)

```{literalinclude} ../../../hosgdSims/HoSGDdefs.py
:language: python
:lines: 1-2
```
```{literalinclude} ../../../hosgdSims/HoSGDdefs.py
:language: python
:pyobject: lrSGD
```
~~~

</details>

````

<hr style="border:2px solid gray"> </hr>

<span style="font-size:1.5em;"><strong>Simulation</strong></span>

<details>
  <summary><span style="font-size:1.25em;">1. Load dataset</span></summary>

```{literalinclude} ../../../hosgdSims/HoSGDlabSGD.py
:language: python
:pyobject: stdPreproc
```

```python
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.datasets import mnist
from tensorflow.keras.datasets import cifar10

(X, Y), (Xtest, Ytest) = mnist.load_data()
#(X, Y), (Xtest, Ytest) = cifar10.load_data()

# standard pre-processing
X     = stdPreproc(X)
Xtest = stdPreproc(Xtest)


# if needed --> data vectorization
X     = np.transpose( X.reshape(X.shape[0], np.array(X.shape[1::]).prod()) )
Xtest = np.transpose( Xtest.reshape(Xtest.shape[0], np.array(Xtest.shape[1::]).prod()) )
Y     = Y.ravel()
Ytest = Ytest.ravel()

```

</details>


<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">2. Load the optimization problem</span></summary>

```python

    # --- Optimization model
    # --- (see "Parameters", "OptProb", "E.g. Softmax"
    #      in the tabbed panel above)
    # ---------------------------------------------------------

    nClasses = 10
    OptProb = hosgdFunSoftMax(X, Y, nClasses, verbose=1, Xtest=Xtest, Ytest=Ytest)

```

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">3. Select the hyper-parameters routines</span></summary>

```python

    # --- It is assumed that class 'lrSGD' is defined
    #     in file 'HoSGDdefs.py'
    # --- (see "Parameters", "hyperFun", "class lrSGD"
    #      in the tabbed panel above)
    # ---------------------------------------------------------

    import HoSGDdefs as hosgdDef


    # --- hyperPars
    # --- (see "Parameters", "hyperFun", "E.g."
    #      in the tabbed panel above)
    # ---------------------------------------------------------

    hyperP = hosgdHyperFunSGD(OptProb, lrPolicy=hosgdDef.lrSGD.Cte)

    # alternatively:
    # hyperP = hosgdHyperFunSGD(OptProb, lrPolicy=lrPolicy, lrSDecay=lrSDecay, lrTau=lrTau)

```

</details>

<div style="line-height:50%;"> <br> </div>


<details>
  <summary><span style="font-size:1.25em;">4. Execute SGD</span></summary>

```python

    # --- nEpoch, blkSize, alpha0: user defined.
    # --- OptProb: see #2 in the current list
    # --- hyperP: see #3 in the current list

    W, stats = hosgdSGD(OptProb, nEpoch, blkSize, alpha, hyperP)

```

</details>

<div style="line-height:50%;"> <br> </div>

<hr style="border:2px solid gray"> </hr>


<span style="font-size:1.5em;"><strong>Full solution</strong></span>

<details>
  <summary><span style="font-size:1.25em;">HoSGDdefs.py</span></summary>

<div style="line-height:50%;"> <br> </div>

```{literalinclude} ../../../hosgdSims/HoSGDdefs.py
:language: python
:lines: 1-2
```
```{literalinclude} ../../../hosgdSims/HoSGDdefs.py
:language: python
:lines: 25-38,58-78
```


</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">HoSGDL1b.py</span></summary>

<div style="line-height:50%;"> <br> </div>

```{literalinclude} ../../../hosgdSims/HoSGDlabSGD.py
:language: python
```


</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">Simple Simulation (MNIST)</span></summary>

```python


import numpy as np
import HoSGDlabSGD as H

# MNIST
w, stats = H.exMultiClass(20, 32, 0.05, dataset='mnist')

```
![Sim](./sgd-ex1a.png "mnist")
![Sim](./sgd-ex1b.png "mnist")

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">Simple Simulation (CIFAR10)</span></summary>

```python


import numpy as np
import HoSGDlabSGD as H

# MNIST
w, stats = H.exMultiClass(20, 32, 0.01, dataset='cifar10')

```

![Sim](./sgd-ex2a.png "cifar-10")
![Sim](./sgd-ex2b.png "cifar-10")

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">Simulation with StepDecay</span></summary>

```python


import numpy as np
import HoSGDlabSGD as H    # Lab1b

# MNIST
w, stats = H.exMultiClass(25, 64, 0.02, dataset='cifar10', lrPolicy=H.hosgdDef.lrSGD.StepDecay, lrSDecay=10, lrTau=0.5)

```

![Sim](./sgd-ex3a.png "StepDecay")
![Sim](./sgd-ex3b.png "StepDecay")

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">Running the simulations within Colab</span></summary>

* Sign-in into [Colab](https://colab.research.google.com/)
* Upload the necessary files; for this example:
  * HoSGDlabGD.py
  * HoSGDlabSGD.py
  * HoSGDdefs.py
  * In Colab, execute

```python
        from google.colab import files
        src = list(files.upload().values())[0]
```

* Proceed as shown above

</details>

<div style="line-height:50%;"> <br> </div>

</details>

<div style="line-height:50%;"> <br> </div>
