
<!-- .dDown[open] > summary {
  box-shadow: 3px 3px 4px black;
  cursor: pointer;
  list-style: none;}

  DarkOliveGreen
  DarkRed
  #283747;  similar (but darker) to
  DarkSlateGray;
-->


<style type="text/css">
<!--
 .tab1 { margin-left: 1em;}
 .tab2 { margin-left: 2em;}
 .dDown[open] > summary {
  color: DarkSlateGray;}
 .dDown > summary {
  color: DarkSlateGray;}
-->
</style>



(theoryGD)=
# GD

Consider cost functional


\begin{eqnarray*}
F(\mathbf{u}) & =  & f(\mbox{Op}(\mathbf{u}), \mathbf{b}) \qquad (1)
\end{eqnarray*}

````{tabbed} where


* $\nabla F$ is $L$-Lipschitz continuous: $\| \nabla F(\mathbf{x}) - \nabla F(\mathbf{y}) \|_2 \leq L(F)\| \mathbf{x} - \mathbf{y} \|_2$,
* $\mathbf{u} \in \mathbb{R}^{n_1 \times n_2 \times \ldots}$,
* $\mbox{Op}(\cdot) : \mathbb{R}^{n_1 \times n_2 \times \ldots} \mapsto \mathbb{R}^L$ represents a forward operator,
* $\mathbf{b} \in \mathbb{R}^L$ represents some vectorized observed/input data.
* $f(\cdot) : \mathbb{R}^L \mapsto \mathbb{R}$ is a loss (cost) function.

````

````{tabbed} simple example

* Quadratic cost along with a linear operator <br>
  \begin{eqnarray*} F(\mathbf{u}) = 0.5 \| A\mathbf{u} - \mathbf{b} \|_2^2\end{eqnarray*}

````

<u>Problem</u>: write you own {hoverxref}`GD<algoGD>` routine; as a test function use a quadratic loss (such the one
considered in the "simple example" tab of the above panel).

(simsolGD)=
<details><summary><strong>Proposed solution</strong></summary>

<div style="line-height:50%;"> <br> </div>

Among other pausible alternatives, the GD algorithm may be written as

````{tabbed} Algorithm

```{literalinclude} ../../../hosgdSims/HoSGDlabGD.py
:language: python
:pyobject: hosgdGD
```

````

````{tabbed} Parameters

```{literalinclude} ../../../hosgdSims/HoSGDlabGD.py
:language: python
:pyobject: hosgdGD
:lines: 1
```

<details><summary><strong>OptProb</strong></summary>

~~~{tabbed} Description
* <code class="language-python">class</code> that encapsultes a particular optimization problem.
* Given the function $F(\cdot) : \mathbb{R}^N \mapsto \mathbb{R}$, this <code class="language-python">class</code> should implement
    * <code class="language-python">costFun</code>: computes the cost functional.
    * <code class="language-python">gradFun</code>: computes the gradient, i.e. $ \nabla F(\cdot) $.
    * <code class="language-python">randSol</code>: generates a random initial solution
    * <code class="language-python">fwOp</code>: operator (function) that maps or transforms variable $\mathbf{u} \in \mathbf{R}^{n_1 \times n_2 \times \ldots}$
      into $\mathbf{R}^N$, e.g. $A \mathbf{u} $, where $A \in \mathbf{R}^{N \times M}$, and $\mathbf{u} \in \mathbf{R}^{M} $.
    * <code class="language-python">computeStats</code>: gathers useful statistics (e.g. cost functional, current step-size value, etc.).
~~~

~~~{tabbed} E.g. quadratic func.
```python
class hosgdFunQlin(hosgdOptProb):
    r""" 0.5|| Au - b ||_2^2

    """

    def __init__(self, A, b, Nstats=3, verbose=10, initSol=None):

        self.A = A
        self.b = b
        self.Nstats   = Nstats
        self.verbose  = verbose
        self.initSol  = initSol      #  if not None --> reproducible initial solution

    # ---------


    # ---------

    def costFun(self, u):

        return 0.5*np.linalg.norm( self.A.dot(u).ravel()-self.b.ravel(), ord=2)**2


    # ---------

    def fwOp(self, u):

        return self.A.dot(u)

    # ---------

    def gradFun(self, u):

        return self.A.transpose().dot(self.A.dot(u) - self.b)

    # ---------

    def randSol(self):

        if self.initSol is None:
           rng = np.random.default_rng()
        else:
           rng = np.random.default_rng(self.initSol)

        return rng.normal(size=[self.A.shape[1],1])


    # ---------

    def computeStats(self, u, alpha, k, g):

        cost  = self.costFun(u)

        return np.array([k, cost, alpha])


    def printStats(self, k, nIter, v):

        if k == 0:
            print('\n')

        if self.verbose > 0:

            if np.remainder(k,self.verbose)==0 or k==nIter-1:
               print('{:>3d}\t {:.3e}\t {:.2e} '.format(int(v[k,0]),v[k,1],v[k,2]))

        return


```
~~~

</details>

<div style="line-height:50%;"> <br> </div>

<details><summary><strong>nIter</strong></summary>
  <div style="line-height:50%;"> <br> </div>
  Total number of iterations.
</details>

<div style="line-height:50%;"> <br> </div>

<details><summary><strong>alpha0</strong></summary>
  <div style="line-height:50%;"> <br> </div>
  Initial step-size.
</details>

<div style="line-height:50%;"> <br> </div>

(hyperFunGD)=
<details><summary><strong>hyperFun</strong></summary>

~~~{tabbed} Description
* <code class="language-python">class</code>  that encapsultes any functionality related to GD's hyper-parameters.
* Up to this point, this <code class="language-python">class</code> would only focus on computing the step-size $\alpha$.
~~~

~~~{tabbed} E.g.
```python
class hosgdHyperFun(object):
    r"""Class related to routines associated to hyperPars computation
    """

    def __init__(self, OptProb, ssPolicy=hosgdDef.ss.Cte):
        self.ssPolicy = ssPolicy
        self.fwOp = OptProb.fwOp
        self.SS   = self.sel_SS()

    def sel_SS(self):
      switcher = {
          hosgdDef.ss.Cte.val:          self.ssCte,
          hosgdDef.ss.Cauchy.val:       self.ssCauchy,
          hosgdDef.ss.CauchyLagged.val: self.ssCauchyLagged,
          hosgdDef.ss.BBv1.val:         self.ssBBv1,
          hosgdDef.ss.BBv2.val:         self.ssBBv2,
          hosgdDef.ss.BBv3.val:         self.ssBBv3,
          }

      ssFun = switcher.get(self.ssPolicy.val, "nothing")

      func = lambda u, alpha, k, g: ssFun(u, alpha, k, g)

      return func

    # --- === ---

    def ssCte(self, u, alpha, k, g):
        return alpha

    # --- Routines below will be implement later.

    def ssCauchy(self, u, alpha, k, g):
        pass

    def ssCauchyLagged(self, u, alpha, k, g):
        pass

    def ssBBv1(self, u, alpha, k, g):
        pass

    def ssBBv2(self, u, alpha, k, g):
        pass

    def ssBBv3(self, u, alpha, k, g):
        pass

```
~~~

~~~{tabbed} class `ss` (step-size)

```{literalinclude} ../../../hosgdSims/HoSGDdefs.py
:language: python
:lines: 1-2
```
```{literalinclude} ../../../hosgdSims/HoSGDdefs.py
:language: python
:pyobject: ss
:lines: 1-5,9-
```
~~~

</details>


````

<hr style="border:2px solid gray"> </hr>

<span style="font-size:1.5em;"><strong>Simulation</strong></span>

<details>
  <summary><span style="font-size:1.25em;">1. Generate input data</span></summary>

```python

    import numpy as np

    # --- Data generation (N,M: user inputs)
    # --------------------------------------
    rng = np.random.default_rng()

    A = rng.normal(size=[N,M])
    D = np.sqrt(max(N,M))*np.diag( rng.random([max(N,M),]), 0)
    A += D[0:N,0:M]
    A /= np.linalg.norm(A,axis=0)

    xOrig = np.random.randn(M,1)

    b = A.dot(xOrig) + sigma*np.random.randn(N,1)

```

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">2. Load the optimization problem</span></summary>

```python

    # --- Optimization model
    # --- (see "Parameters", "OptProb", "E.g. quadratic func."
    #      in the tabbed panel above)
    # ---------------------------------------------------------

    OptProb = hosgdFunQlin(A,b)

```

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">3. Select the hyper-parameters routines</span></summary>

```python

    # --- It is assumed that class 'ss' is defined
    #     in file 'HoSGDdefs.py'
    # --- (see "Parameters", "hyperFun", "class ss"
    #      in the tabbed panel above)
    # ---------------------------------------------------------

    import HoSGDdefs as hosgdDef


    # --- hyperPars
    # --- (see "Parameters", "hyperFun", "E.g."
    #      in the tabbed panel above)
    # ---------------------------------------------------------

    hyperP = hosgdHyperFun(OptProb, ssPolicy=hosgdDef.ss.Cte)

```

</details>

<div style="line-height:50%;"> <br> </div>


<details>
  <summary><span style="font-size:1.25em;">4. Execute GD</span></summary>

```python

    # --- nIter, alpha0: user defined.
    # --- OptProb: see #2 in the current list
    # --- hyperP: see #3 in the current list

    u, stats = hosgdGD(OptProb, nIter, alpha0, hyperP)

```

</details>

<div style="line-height:50%;"> <br> </div>

<hr style="border:2px solid gray"> </hr>

<span style="font-size:1.5em;"><strong>Full solution</strong></span>

<details>
  <summary><span style="font-size:1.25em;">HoSGDdefs.py</span></summary>

<div style="line-height:50%;"> <br> </div>

```{literalinclude} ../../../hosgdSims/HoSGDdefs.py
:language: python
:lines: 1-2
```
```{literalinclude} ../../../hosgdSims/HoSGDdefs.py
:language: python
:lines: 1-5,14-23,58-79
```


</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">class `OptProb` (bare-bones)</span></summary>


```{literalinclude} ../../../hosgdSims/HoSGDdefs.py
:language: python
:pyobject: hosgdOptProb
```
</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">HoSGDL1a.py</span></summary>

<div style="line-height:50%;"> <br> </div>

```{literalinclude} ../../../hosgdSims/HoSGDlabGD.py
:language: python
:lines: 1-26,30-45,143-
```


</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">Simple Simulation</span></summary>


```python

import numpy as np
import HoSGDlabGD as H

# run simple example
u, st = H.exQlin(40, 0.01, N=500, M=2000)

```

![Sim](./gd-ex1.png "Simple simulation")

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">Using several step-sizes</span></summary>

```python

alpha = np.array([0.05, 0.01, 0.001])

# run example with several ss
u, st = H.exQlin(40, alpha, N=500, M=2000)

```
![Sim](./gd-ex2.png "Several step-sizes")

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">Running the simulations within Colab</span></summary>

* Sign-in into [Colab](https://colab.research.google.com/)
* Upload the necessary files; for this example:
  * HoSGDlabGD.py
  * HoSGDdefs.py
  * In Colab, execute

```python
        from google.colab import files
        src = list(files.upload().values())[0]
```

* Proceed as shown above

</details>


<!-- Global details -->

</details>

<div style="line-height:50%;"> <br> </div>

