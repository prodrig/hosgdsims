
Lecture 1
=========


.. collapse:: Theory

  Lecture slides: `Here <https://drive.google.com/file/d/19Qxx9f7v8b1dxD_S8tO8q0hGGq1u_y8j/view?usp=sharing>`_

.. raw:: html

    <hr style="border:2px solid gray"> </hr>


These hands-on exercesises focus on first implementing the Gradient Descent (GD) algorithm
to then continue with the Stochastic GD (SGD) implementation. For the latter the MNIST / CIFAR-10 datasets will be used
to illustrate a multiclass classification problem.



.. toctree::
   :maxdepth: 1
   :caption: Simulations

   GD.md
   SGD.md
