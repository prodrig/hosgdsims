
<!-- .dDown[open] > summary {
  box-shadow: 3px 3px 4px black;
  cursor: pointer;
  list-style: none;}

  DarkOliveGreen
  DarkRed
  #283747;  similar (but darker) to
  DarkSlateGray;
-->


<style type="text/css">
<!--
 .tab1 { margin-left: 1em;}
 .tab2 { margin-left: 2em;}
 .dDown[open] > summary {
  color: DarkSlateGray;}
 .dDown > summary {
  color: DarkSlateGray;}
-->
</style>


(theoryAGD)=
# AGD

<span style="font-size:1.5em;"><strong>Adpative step-size</strong></span>

Considering a cost functional with the same characteristics as the one used for the {hoverxref}`GD<theoryGD>` case, then

````{tabbed} Problem (1)
* augment the {hoverxref}`hyperFunGD<hyperFunGD>` <code class="language-python">class</code> by implementing some adpative step-size methods:
  * Cauchy step-size.
  * Barzilai-Borwein (BB), all three versions.
  * Cauchy-BB.
````

````{tabbed} Recall
When solving the GD simulation, the following classes / routines were considered

<span style="display:block; margin-top:-20px;"></span>
* <details><summary>class <code class="language-python">ss</code> (step-size)</summary>
  <div style="line-height:50%;"> <br> </div>

  ```{literalinclude} ../../../hosgdSims/HoSGDdefs.py
  :language: python
  :lines: 1-2
  ```
  ```{literalinclude} ../../../hosgdSims/HoSGDdefs.py
  :language: python
  :pyobject: ss
  :lines: 1-5,9-
  ```
  </details>

* <details><summary>class <code class="language-python">hyperFun</code> </summary>
  <div style="line-height:50%;"> <br> </div>

  ```{literalinclude} ../../../hosgdSims/sphinxHyperFun.py
  :language: python
  :emphasize-lines: 33,36
  ```
  </details>

````

````{tabbed} Cauchy
Given the cost functional $\displaystyle f(\mathbf{u}) = 0.5\| \Phi \mathbf{u} - \mathbf{b} \|_2^2 $ the Cauchy
step-size is computed via (among others, see {cite:ps}`yuan-2008-stepsizes`)
\begin{eqnarray*}
  \alpha & = & \frac{\| \mathbf{g} \|_2^2}{ \| \Phi \mathbf{g} \|_2^2 },
\end{eqnarray*}
where $\mathbf{g} = \nabla f(\mathbf{u})$.

````

````{tabbed} BB
Given the cost functional $\displaystyle f(\mathbf{u})$ and the vectors  $\mathbf{s}_k$ and $\mathbf{y}_k$, where
$ \begin{array}{ll}
\bullet & \mathbf{s}_k = \mathbf{u}_k - \mathbf{u}_{k-1}. \\
\bullet & \mathbf{y}_k = \mathbf{g}_k - \mathbf{g}_{k-1}. \\
\bullet & \mathbf{u}_k \mbox{ is the solution at iteration }k. \\
\bullet & \mathbf{g}_k =\nabla f(\mathbf{u}_k)
\end{array}
$

then (see {cite:ps}`barzilai-1988-two` and {cite:ps}`li-2019-new` )

$$\begin{array}{rcll}
  \alpha_{1} & = & \displaystyle \frac{\| \mathbf{s}_k \|_2^2}{ \langle \mathbf{s}_k, \mathbf{y}_k\rangle } & \qquad \color{darkgray}\mbox{labeled ``BBv1''}\\
  & & \\
  \alpha_{2} & = & \displaystyle \frac{\langle \mathbf{s}_k, \mathbf{y}_k\rangle }{\| \mathbf{y}_k \|_2^2} & \qquad \color{darkgray}\mbox{labeled ``BBv2''} \\
  & & \\
  \alpha_{3} & = & \displaystyle \sqrt{ \alpha_1 \cdot \alpha_2 } &  \\
   & = & \displaystyle  \frac{ \| \mathbf{s}_k \|_2 }{\| \mathbf{y}_k \|_2} & \qquad \color{darkgray}\mbox{labeled ``BBv3''}
\end{array}
$$


````

````{tabbed} Cauchy-BB

This method is also called Cauchy ``lagged'': Given the cost functional $\displaystyle f(\mathbf{u})$
and initial solution $\mathbf{x}_0$, then (see {cite:ps}`raydan-2002-relaxed`)

$\begin{array}{l}
\mbox{for } k = 0,\,1,\, \ldots  \\
\begin{array}{lll}
 & \mbox{if } REM(\,(k,\,2) == 0 &
\end{array} \\
\begin{array}{lll}
 & & \alpha_k = \alpha_{k}^{\mbox{C}} \qquad\qquad{\color{lightgray} \mbox{ Cauchy step-size}} \\
\end{array} \\
\begin{array}{lll}
 & \mbox{else}
\end{array} \\
\begin{array}{lll}
 & & \alpha_k = \alpha_{k\mbox{-}1}  \qquad\qquad{\color{lightgray} \mbox{BB}} \\
\end{array} \\
\begin{array}{lll}
 & \\
 & \mathbf{x}_{k\mbox{+}1}  =  \mathbf{x}_{k} - \alpha_k \nabla \mathbf{f}_{k} \\
\end{array} \\
\end{array}
$

````

<details><summary><strong>Proposed solution</strong></summary>

Among other pausible alternatives, and assuming the optimization problem is given by $F(\mathbf{u}_k) = f( \mbox{fwOp}(\mathbf{u}_k), b)$, where

* <code class="language-python">g</code>: variable that represents $\mathbf{g}_k = \nabla F(\mathbf{u}_k)$,
* <code class="language-python">self.fwOp(g)</code> computes $f( \mbox{fwOp}(\mathbf{g}_k), b)$,

the considered adaptive step-size methods may be written as

````{tabbed} Cauchy
```{literalinclude} ../../../hosgdSims/HoSGDlabGD.py
:language: python
:pyobject: hosgdHyperFun
:lines: 35-40
```

````

````{tabbed} BB
```{literalinclude} ../../../hosgdSims/HoSGDlabGD.py
:language: python
:pyobject: hosgdHyperFun
:lines: 54-127
```

````

````{tabbed} Cauchy-BB
```{literalinclude} ../../../hosgdSims/HoSGDlabGD.py
:language: python
:pyobject: hosgdHyperFun
:lines: 44-51
```

````

<hr style="border:2px solid gray"> </hr>

<span style="font-size:1.5em;"><strong>Simulation</strong></span>

<details>
  <summary><span style="font-size:1.25em;">1. Generate input data</span></summary>

```python

    import numpy as np

    # --- Data generation (N,M: user inputs)
    # --------------------------------------
    rng = np.random.default_rng()

    A = rng.normal(size=[N,M])
    D = np.sqrt(max(N,M))*np.diag( rng.random([max(N,M),]), 0)
    A += D[0:N,0:M]
    A /= np.linalg.norm(A,axis=0)

    xOrig = np.random.randn(M,1)

    b = A.dot(xOrig) + sigma*np.random.randn(N,1)

```

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">2. Load the optimization problem</span></summary>

```python

    # --- Optimization model
    # --- (see "Parameters", "OptProb", "E.g. quadratic func."
    #      in the tabbed panel above)
    # ---------------------------------------------------------

    OptProb = hosgdFunQlin(A,b)

```

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">3. Select the adaptive step-size method</span></summary>


```python

    # --- It is assumed that class 'ss' is defined
    #     in file 'HoSGDdefs.py'
    # --- (see "Parameters", "hyperFun", "class ss"
    #      in the tabbed panel above)
    # ---------------------------------------------------------

    import HoSGDdefs as hosgdDef


    # --- hyperPars
    # --- (see "Parameters", "hyperFun", "E.g."
    #      in the tabbed panel above)
    # ---------------------------------------------------------

    # Cte. SS:
    hyperP = hosgdHyperFun(OptProb, ssPolicy=hosgdDef.ss.Cte)

    # Cauchy SS:
    hyperP_C = hosgdHyperFun(OptProb, ssPolicy=hosgdDef.ss.Cauchy)

    # Cauchy BBv1 SS:
    hyperP_BB1 = hosgdHyperFun(OptProb, ssPolicy=hosgdDef.ss.BBv1)

    # Cauchy-BB SS:
    hyperP_CBB = hosgdHyperFun(OptProb, ssPolicy=hosgdDef.ss.CauchyLagged)

    # etc.

```

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">4. Execute GD + adaptive SS</span></summary>

```python

    # --- nIter, alpha0: user defined.
    # --- OptProb: see #2 in the current list
    # --- hyperP / ssPolicy: see #3 in the current list

    # Cte. SS:
    u, stats = hosgdGD(OptProb, nIter, alpha, hyperP)

    # Cauchy SS:
    u1, stats1 = hosgdGD(OptProb, nIter, alpha, hyperP_C)

    # Cauchy BBv1:
    u2, stats2 = hosgdGD(OptProb, nIter, alpha, hyperP_BB1)

    # Cauchy-BB SS:
    u3, stats3 = hosgdGD(OptProb, nIter, alpha, hyperP_CBB)

```

</details>


<hr style="border:2px solid gray"> </hr>

<span style="font-size:1.5em;"><strong>Full solution</strong></span>

<details>
  <summary><span style="font-size:1.25em;">HoSGDdefs.py</span></summary>

<div style="line-height:50%;"> <br> </div>

```{literalinclude} ../../../hosgdSims/HoSGDdefs.py
:language: python
:lines: 1-2
```
```{literalinclude} ../../../hosgdSims/HoSGDdefs.py
:language: python
:pyobject: ss
:lines: 1-5,9-
```


</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">HoSGDL2gd.py</span></summary>

<div style="line-height:50%;"> <br> </div>

```{literalinclude} ../../../hosgdSims/HoSGDlabGD.py
:language: python
:lines: 1-26,30-
```


</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">Simple Simulation</span></summary>

```python


import numpy as np
import HoSGDlabGD as H
import HoSGDdefs as hosgdDef

# set different ssPolicies
ssPolicy = []
ssPolicy.append( hosgdDef.ss.Cte )
ssPolicy.append( hosgdDef.ss.Cauchy )
ssPolicy.append( hosgdDef.ss.CauchyLagged )
ssPolicy.append( hosgdDef.ss.BBv1 )
ssPolicy.append( hosgdDef.ss.BBv2 )

u, st = H.exQlin(40, 0.01, N=500, M=2000, ssPolicy=ssPolicy)

```

![Sim](./agd-ex1a.png "Simple simulation")
![Sim](./agd-ex1b.png "Simple simulation")

</details>


</details>


<hr style="border:2px solid #2980b9"> </hr>
<hr style="margin-top:-20px;border:2px solid #2980b9; "> </hr>


<span style="font-size:1.5em;"><strong>Accelerted GD: Momentum \& Nesterov</strong></span>

````{tabbed} Problem (2)
* implement two accelerated versions of GD, namely
  * Momentum (a.k.a. Polyak's heavy ball, see {cite:ps}`polyak-1964-some`) acceleration.
  * Nesterov acceleration {cite:ps}`nesterov-1983-method`.
````

````{tabbed} Momentum
Given the cost functional $\displaystyle F(\mathbf{u}) = f(\mbox{Op}(\mathbf{u}), \mathbf{b}) $
 and parameter $\gamma$ then

$$ \begin{array}{lcl}
%
  \phantom{Momentum}\mbox{GD} & : &  \begin{array}{rcl}\mathbf{u}_{k+1} & = & \mathbf{u}_{k} - \alpha \cdot \nabla F(\mathbf{u}_{k}) \end{array} \\
%
    & & \\
%
  \phantom{GD}\mbox{Momentum} & : &  \begin{array}{rcl}
            \mathbf{z}_{k+1} & = & \gamma\cdot\mathbf{z}_{k} - \alpha \cdot \nabla F(\mathbf{u}_{k}) \\
            \mathbf{u}_{k+1} & = & \mathbf{u}_{k} + \mathbf{z}_{k+1}
                                     \end{array} \\
\end{array}
$$

````

````{tabbed} Nesterov
Given the cost functional $\displaystyle F(\mathbf{u}) = f(\mbox{Op}(\mathbf{u}), \mathbf{b}) $ and the inertial sequence $\{ \gamma_k \}$ then

$$ \begin{array}{lcl}
%
  \phantom{Nesterov}\mbox{GD} & : &  \begin{array}{rcl}\mathbf{u}_{k+1} & = & \mathbf{u}_{k} - \alpha \cdot \mathbf{g}_k \end{array} \\
%
    & & \\
%
  \phantom{GD}\mbox{Nesterov} & : &  \begin{array}{rcl}
            \mathbf{u}_{k+1} & = & \mathbf{y}_{k} - \alpha \cdot \nabla F(\mathbf{y}_{k}) \\
            \mathbf{y}_{k+1} & = & \mathbf{u}_{k} + \gamma_k\cdot(\mathbf{u}_{k+1} - \mathbf{u}_{k})
                                     \end{array} \\
\end{array}
$$

````

<details><summary><strong>Proposed solution</strong></summary>

<div style="line-height:50%;"> <br> </div>

Among other pausible alternatives, the considered accelerated GD algorithms may be written as

````{tabbed} Momentum

Highlighted lines point out the differences between {hoverxref}`GD<theoryGD>` and Momentum {cite:ps}`polyak-1964-some`.

```{literalinclude} ../../../hosgdSims/HoSGDlabAGD.py
:language: python
:pyobject: hosgdMTM
:emphasize-lines: 13,15
```

````

````{tabbed} Nesterov

Highlighted lines point out the differences between {hoverxref}`GD<theoryGD>` and Nesterov {cite:ps}`nesterov-1983-method`.

```{literalinclude} ../../../hosgdSims/HoSGDlabAGD.py
:language: python
:pyobject: hosgdNTRV
:emphasize-lines: 10,17,19
```

````

````{tabbed} Parameters

```{literalinclude} ../../../hosgdSims/HoSGDlabAGD.py
:language: python
:lines: 70-71, 94
```

<details><summary><strong>OptProb</strong></summary>
    Blah, blah
</details>

<div style="line-height:50%;"> <br> </div>

<details><summary><strong>nIter</strong></summary>
  <div style="line-height:50%;"> <br> </div>
  Total number of iterations.
</details>

<div style="line-height:50%;"> <br> </div>

<details><summary><strong>alpha0</strong></summary>
  <div style="line-height:50%;"> <br> </div>
  Initial step-size.
</details>

<div style="line-height:50%;"> <br> </div>

<details><summary><strong>hyperFun</strong></summary>
    Blah, blah
</details>

````

<hr style="border:2px solid gray"> </hr>

<span style="font-size:1.5em;"><strong>Simulation</strong></span>

<details>
  <summary><span style="font-size:1.25em;">1. Generate input data</span></summary>

```python

    import numpy as np

    # --- Data generation (N,M: user inputs)
    # --------------------------------------
    rng = np.random.default_rng()

    A = rng.normal(size=[N,M])
    D = np.sqrt(max(N,M))*np.diag( rng.random([max(N,M),]), 0)
    A += D[0:N,0:M]
    A /= np.linalg.norm(A,axis=0)

    xOrig = np.random.randn(M,1)

    b = A.dot(xOrig) + sigma*np.random.randn(N,1)

```

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">2. Load the optimization problem</span></summary>

```python

    # --- Optimization model
    # --- (see "Parameters", "OptProb", "E.g. quadratic func."
    #      in the tabbed panel above)
    # ---------------------------------------------------------

    OptProb = hosgdFunQlin(A,b)

```

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">3. Select the hyper-parameters routines</span></summary>

```python

    # --- It is assumed that class 'ss' is defined
    #     in file 'HoSGDdefs.py'
    # --- (see "Parameters", "hyperFun", "class ss"
    #      in the tabbed panel above)
    # ---------------------------------------------------------

    import HoSGDdefs as hosgdDef


    # --- hyperPars
    # --- (see "Parameters", "hyperFun", "E.g."
    #      in the tabbed panel above)
    # ---------------------------------------------------------

    # Momentum case:
    hyperPmtm = hosgdHyperFun(OptProb, ssPolicy=hosgdDef.ss.Cte, mtmGamma=0.9)

    # Nesterov case:
    hyperPntrv = hosgdHyperFun(OptProb, ssPolicy=hosgdDef.ss.Cte, iseqPolicy=hosgdDef.iseq.Ntrv)

```

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">4. Execute Momentum or Nesterov</span></summary>

```python

    # --- nIter, alpha0: user defined.
    # --- OptProb: see #2 in the current list
    # --- hyperP: see #3 in the current list

    # Momentum case:
    u1, stats1 = hosgdMTM(OptProb, nIter, alpha0, hyperPmtm)

    # Nesterov case:
    u2, stats2 = hosgdNTRV(OptProb, nIter, alpha0, hyperPntrv)

```

</details>

<div style="line-height:50%;"> <br> </div>

<hr style="border:2px solid gray"> </hr>

<div style="line-height:50%;"> <br> </div>


<span style="font-size:1.5em;"><strong>Full solution</strong></span>

<details>
  <summary><span style="font-size:1.25em;">HoSGDdefs.py</span></summary>

<div style="line-height:50%;"> <br> </div>

```{literalinclude} ../../../hosgdSims/HoSGDdefs.py
:language: python
:lines: 1-23, 40-53
```


</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">HoSGDL2agd.py</span></summary>

<div style="line-height:50%;"> <br> </div>

```{literalinclude} ../../../hosgdSims/HoSGDlabAGD.py
:language: python
:lines: 1-26,30-
```


</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">Simple Simulation</span></summary>

```python

import numpy as np
import HoSGDlabAGD as H
import HoSGDdefs as hosgdDef

# run defualt sim: compare GD, MTM and NTRV

u, st = H.exQlinAGD(40, 0.01, N=500, M=2000)

```

![Sim](./agd-ex2.png "Simple simulation")

</details>


</details>

<div style="line-height:50%;"> <br> </div>
