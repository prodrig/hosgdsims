
<!-- .dDown[open] > summary {
  box-shadow: 3px 3px 4px black;
  cursor: pointer;
  list-style: none;}

  DarkOliveGreen
  DarkRed
  #283747;  similar (but darker) to
  DarkSlateGray;
-->


<style type="text/css">
<!--
 .tab1 { margin-left: 1em;}
 .tab2 { margin-left: 2em;}
 .dDown[open] > summary {
  color: DarkSlateGray;}
 .dDown > summary {
  color: DarkSlateGray;}
-->
</style>



# SGD variants

Considering a cost functional with the same characteristics as the one used for the {hoverxref}`SGD<theorySGD>` case, then
````{tabbed} Problem (1)
* modify the {hoverxref}`SGD<routineSGD>` <code class="language-python">routine</code> to implement some SGD variants:
  * Momentum {cite:ps}`polyak-1964-some`.
  * RMSprop {cite:ps}`hinton-2012-neural`.
  * ADAM {cite:ps}`kingma-2015-adam`.
````

````{tabbed} Recall
When solving the SGD simulation, the following classes / routines were considered

<span style="display:block; margin-top:-20px;"></span>

* <details><summary> <code class="language-python">honsgdSGD</code> </summary>
  <div style="line-height:50%;"> <br> </div>

  ```{literalinclude} ../../../hosgdSims/HoSGDlabSGD.py
  :language: python
  :pyobject: hosgdSGD
  ```

  </details>


````

````{tabbed} Momentum
Using the {hoverxref}`Generalized SGD<algoGenSGD>` as a reference (in particular the "$\mathbf{\mbox{for }} b$" loop),
and assuming $\beta$ is a given value, then

$$ \begin{array}{l}
\begin{array}{llllll}
 & & \mathbf{v} & = & 1 & \qquad \color{darkgray}  \\
  & & \mathbf{w} & = & 1 & \qquad \color{darkgray}  \\
  & & \nabla & = & \mathbf{g} & \qquad \color{darkgray}  \\
 & & \mathbf{W}_{k+1} & = & \mathbf{W}_k - \alpha \cdot \nabla + \beta\cdot(\mathbf{W}_{k}-\mathbf{W}_{k-1}) & \qquad
\end{array} \\
% ---
\end{array}
$$


````

````{tabbed} RMSprop
Using the {hoverxref}`Generalized SGD<algoGenSGD>` as a reference (in particular the "$\mathbf{\mbox{for }} b$" loop),
and assuming $\gamma_2$ and $\epsilon$ are given values, then

$$ \begin{array}{l}
\begin{array}{llllll}
 & & \mathbf{v} & = & \gamma_2\cdot \mathbf{v} + (1-\gamma_2)\cdot \mathbf{g} \odot \mathbf{g} & \qquad \color{darkgray}  \\
  & & \mathbf{w} & = & \frac{1}{\sqrt{\epsilon + \mathbf{v}}} & \qquad \color{darkgray}  \\
  & & \nabla & = & \mathbf{g} & \qquad \color{darkgray}  \\
 & & \mathbf{W}_{k+1} & = & \mathbf{W}_k - \alpha \cdot \mathbf{w} \odot \nabla  & \qquad
\end{array} \\
% ---
\end{array}
$$


````

````{tabbed} ADAM
Using the {hoverxref}`Generalized SGD<algoGenSGD>` as a reference (in particular the "$\mathbf{\mbox{for }} b$" loop),
and assuming $\gamma_1$, $\gamma_2$ and $\epsilon$ are given values, then

$$ \begin{array}{l}
\begin{array}{llllll}
 & & \mathbf{v} & = & \gamma_2\cdot \mathbf{v} + (1-\gamma_2)\cdot \mathbf{g} \odot \mathbf{g} & \qquad \color{darkgray}  \\
  & & \mathbf{w} & = & \frac{c_k}{\sqrt{\epsilon + \mathbf{v}}} & \qquad \color{darkgray} c_k = \frac{\sqrt{1-\gamma_2^k}}{1-\gamma_1^k} \\
  & & \nabla & = & \gamma_1\cdot\nabla + (1-\gamma_1)\cdot \mathbf{g} & \qquad \color{darkgray}  \\
 & & \mathbf{W}_{k+1} & = & \mathbf{W}_k - \alpha \cdot \mathbf{w} \odot \nabla  & \qquad
\end{array} \\
% ---
\end{array}
$$


````


<details><summary><strong>Proposed solution</strong></summary>

<div style="line-height:50%;"> <br> </div>

Among other pausible alternatives, the considered SGD variants may be written as

````{tabbed} Momentum

Highlighted lines point out the differences between {hoverxref}`SGD<theorySGD>` and Momentum {cite:ps}`polyak-1964-some`.

```{literalinclude} ../../../hosgdSims/HoSGDlabSGDVariants.py
:language: python
:pyobject: hosgdMTM
:emphasize-lines: 39-40
```

````

````{tabbed} RMSProp

Highlighted lines point out the differences between {hoverxref}`SGD<theorySGD>` and RMSProp {cite:ps}`hinton-2012-neural`.

```{literalinclude} ../../../hosgdSims/HoSGDlabSGDVariants.py
:language: python
:pyobject: hosgdRMSProp
:emphasize-lines: 38,40
```

````

````{tabbed} ADAM

Highlighted lines point out the differences between {hoverxref}`SGD<theorySGD>` and ADAM {cite:ps}`kingma-2015-adam`.

```{literalinclude} ../../../hosgdSims/HoSGDlabSGDVariants.py
:language: python
:pyobject: hosgdADAM
:emphasize-lines: 39,40,42
```

````


<div style="line-height:50%;"> <br> </div>
<hr style="border:2px solid gray"> </hr>


<span style="font-size:1.5em;"><strong>Full solution</strong></span>


<details>
  <summary><span style="font-size:1.25em;">HoSGDlabSGDVariants.py</span></summary>

<div style="line-height:50%;"> <br> </div>

```{literalinclude} ../../../hosgdSims/HoSGDlabSGDVariants.py
:language: python
:lines: 3-24
```
```{literalinclude} ../../../hosgdSims/HoSGDlabSGDVariants.py
:language: python
:lines: 31-181
```


</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">Routines to execute example and plot results</span></summary>

<div style="line-height:50%;"> <br> </div>

```{literalinclude} ../../../hosgdSims/HoSGDlabSGDVariants.py
:language: python
:lines: 185-274
```
```{literalinclude} ../../../hosgdSims/HoSGDlabSGDVariants.py
:language: python
:lines: 277-306
```


</details>

<div style="line-height:50%;"> <br> </div>


<details>
  <summary><span style="font-size:1.25em;">Simple Simulation (MNIST)</span></summary>

```python

import HoSGDlabSGDVariants as SGDvar

# MNIST
# exMultiClass: explicitly executes SGD, SGD+M, RMSProp and ADAM all with the
#               same parameters.
sol = SGDvar.exMultiClass(20, 64, 0.001, dataset='mnist',verbose=-1)

```

![Sim](./sgdvar-ex1a.png "mnist")
![Sim](./sgdvar-ex1b.png "mnist")

</details>


<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">Simple Simulation (CIFAR-10)</span></summary>

```python

import HoSGDlabSGDVariants as SGDvar

# MNIST
# exMultiClass: explicitly executes SGD, SGD+M, RMSProp and ADAM all with the
#               same parameters.
sol = SGDvar.exMultiClass(20, 128, 0.001, dataset='cifar10',verbose=-1)

```

![Sim](./sgdvar-ex2a.png "cifar10")
![Sim](./sgdvar-ex2b.png "cifar10")

</details>


</details>

<div style="line-height:50%;"> <br> </div>
