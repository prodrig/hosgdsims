
<!-- .dDown[open] > summary {
  box-shadow: 3px 3px 4px black;
  cursor: pointer;
  list-style: none;}

  DarkOliveGreen
  DarkRed
  #283747;  similar (but darker) to
  DarkSlateGray;
-->


<style type="text/css">
<!--
 .tab1 { margin-left: 1em;}
 .tab2 { margin-left: 2em;}
 .dDown[open] > summary {
  color: DarkSlateGray;}
 .dDown > summary {
  color: DarkSlateGray;}
-->
</style>


(theoryOwnTF)=
# Implementing your own TF solver
````{tabbed} Problem
* Based on [Lecture 4](https://drive.google.com/file/d/1UVCxdXBMZf3k2CW8QvOtNx9Yrbls96xP/view?usp=sharing)
implement your own TF solver
  * Hint: See also [TF's repository](https://github.com/keras-team/keras/tree/v2.12.0/keras/optimizers) for additional examples
````

````{tabbed} Running a simple TF network

<details>
  <summary><span style="font-size:1.0em;">Import required objecs</span></summary>

~~~{tabbed} Standard

```{literalinclude} ../../../hosgdSims/HoSGDlabTF.py
:language: python
:lines: 1-6
```
~~~

~~~{tabbed} TensorFlow / Keras

```{literalinclude} ../../../hosgdSims/HoSGDlabTF.py
:language: python
:lines: 8-30
```
~~~

~~~{tabbed} Progress bar

Used to avoid cluttering the output

```{literalinclude} ../../../hosgdSims/HoSGDlabTF.py
:language: python
:lines: 32-33
```
~~~

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.0em;">Create a simple TF network</span></summary>

```{literalinclude} ../../../hosgdSims/HoSGDlabTF.py
:language: python
:pyobject: create_SimpleNet
```

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.0em;">Define some utility functions</span></summary>

~~~{tabbed} Pre-processing

```{literalinclude} ../../../hosgdSims/HoSGDlabTF.py
:language: python
:pyobject: stdPreproc
```

~~~

~~~{tabbed} Loading MNIST / CIFAR10

```{literalinclude} ../../../hosgdSims/HoSGDlabTF.py
:language: python
:pyobject: load_mnist
```
```{literalinclude} ../../../hosgdSims/HoSGDlabTF.py
:language: python
:pyobject: load_cifar10
```

~~~

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.0em;">Defining a "lr" scheduler </span></summary>

```{literalinclude} ../../../hosgdSims/HoSGDlabTF.py
:language: python
:pyobject: schedulerBLK20
```

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.0em;">Setting up the network and solvers </span></summary>

```{literalinclude} ../../../hosgdSims/HoSGDlabTF.py
:language: python
:pyobject: HoSGD_testNetwork
```

</details>

<div style="line-height:50%;"> <br> </div>

<details open>
  <summary><span style="font-size:1.0em;">Simple simulation </span></summary>

```python

import HoSGDlabTF as hosgdTF

sol = hosgdTF.HoSGD_TestNetwork(0.001, 50, 128)

```

![Sim](./TF_ex1a.png "elm")
![Sim](./TF_ex1b.png "elm")

</details>

<div style="line-height:50%;"> <br> </div>

````


<details><summary><strong>Proposed solution</strong></summary>

<div style="line-height:50%;"> <br> </div>

Among other possible choices, the SGD Momentum variant algorithm is implemented below

<hr style="border:2px solid gray"> </hr>


<span style="font-size:1.5em;"><strong>Full solution</strong></span>

<details>
  <summary><span style="font-size:1.25em;">Own solver ("basic" Momentum)</span></summary>

```{literalinclude} ../../../hosgdSims/HoSGDlabOwnTF.py
:language: python
:pyobject: HoSGD_sgdM
```

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">Setting up the network and solvers </span></summary>

```{literalinclude} ../../../hosgdSims/HoSGDlabOwnTF.py
:language: python
:pyobject: HoSGD_TestOwnNetwork
```

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">Simple simulation</span></summary>

```python

import HoSGDlabOwnTF as hosgdOTF

sol = hosgdOTF.HoSGD_TestOwnNetwork(0.001, 50, 128)

```
![Sim](./TF_ex2a.png "elm")
![Sim](./TF_ex2b.png "elm")

</details>

<div style="line-height:50%;"> <br> </div>


<!--  Last -->
</details>


<div style="line-height:50%;"> <br> </div>
