
<!-- .dDown[open] > summary {
  box-shadow: 3px 3px 4px black;
  cursor: pointer;
  list-style: none;}

  DarkOliveGreen
  DarkRed
  #283747;  similar (but darker) to
  DarkSlateGray;
-->


<style type="text/css">
<!--
 .tab1 { margin-left: 1em;}
 .tab2 { margin-left: 2em;}
 .dDown[open] > summary {
  color: DarkSlateGray;}
 .dDown > summary {
  color: DarkSlateGray;}
-->
</style>


(theoryHL)=
# Hidden layer / ELM

Considering a cost functional with the same characteristics as the one used for the {hoverxref}`SGD<theorySGD>` case, then
````{tabbed} Problem
* Modify the Softmax specialization of the hosgdOptProb class so as to:
  * It accepts an optimization problem which includes a hidden layer (not trainable)
  * Add different activation functions, e.g. ReLU, ELU, etc.
````


(theoryDetail)=
````{tabbed} In detail:

Previously, the Softmax function (for a full explanation see [Lecture 3b's introduction](https://drive.google.com/file/d/1Kt1wPgYzIvMYtM19ft-XrSqrZ-Td43jb/view?usp=sharing)) has been written as:

\begin{eqnarray*} F(\mathbf{W}) & = & \sum_{n=0}^N \langle \mathbf{x}_n, \mathbf{w}_{_{\mathbf{\cal{L}}_n}} \rangle +
                      \sum_{n=0}^N \log\left( \sum_{l=0}^{L-1}
                      \mbox{e}^{ -\langle \mathbf{x}_n, \mathbf{w}_{l} \rangle }\right) \qquad (1) \end{eqnarray*}
where
* $\mathbf{X} = [ \mathbf{x}_1, \mathbf{x}_2, \ldots, \mathbf{x}_N ] \in \mathbb{R}^{r_0 \times N}$ represents the training dataset.
* $\mathbf{W} \in \mathbb{R}^{r_0 \times L}$ represents the weights (to be trained); furthermore, $\mathbf{w}_l$, the $l^{th}$-column of
matrix $\mathbf{W}$, is associated with $l^{th}$-class of the classification problem.
* $L$ is the number of classes associated with the datatset $\mathbf{X}$.
* $\mathbf{w}_{_{\mathbf{\cal{L}}_n}}$: knowing that the input data $\mathbf{x}_n$ is class ${\mathbf{\cal{L}}}_n$, then $\mathbf{w}_{_{\mathbf{\cal{L}}_n}}$
represents the corresponding column in matrix $\mathbf{W}$.

For this particular simulation, first let (1) be written as

\begin{eqnarray*} F(\mathbf{W}) & = & \sum_{n=0}^N \langle \mathbf{X}\mathbf{c}_n^{^{(\mathbf{x})}}, \mathbf{W}\mathbf{h}_n \rangle +
                      \sum_{n=0}^N \log\left( \sum_{l=0}^{L-1}
                      \mbox{e}^{ -\langle \mathbf{X}\mathbf{c}_n^{^{(\mathbf{x})}}, \mathbf{W}\mathbf{c}_l \rangle }\right) \qquad (2) \end{eqnarray*}
where
* $\mathbf{c}_n^{^{(\mathbf{x})}}$ represents a canonical vector (value one at position $n$, zero elsewhere) of length $\mathbf{X}.\mbox{shape}[1]$
* $\mathbf{c}_l$ represents a canonical vector of length L
* $\mathbf{h}_n \in \mathbb{R}^L$ is the one-hot representation of $\mathbf{x}_n$'s class.

Then, by considering $\mathbf{Z}_0 = \mathbf{X}$ and $\mathbf{Z}_1 = \varphi_1(\mathbf{W_0}\mathbf{Z}_0)$, (2) can be written as (3), which in turn may be interpreted as the Softmax function along with a non-trainable hidden layer (which amounts to a particular example of ELM {cite:ps}`huang-2006-universal` ) :

\begin{eqnarray*} F(\mathbf{W_1}) & = & \sum_{n=0}^N \langle \mathbf{Z}_1\mathbf{c}_n^{^{(\mathbf{z})}}, \mathbf{W}_1\mathbf{h}_n \rangle +
                      \sum_{n=0}^N \log\left( \sum_{l=0}^{L-1}
                      \mbox{e}^{ -\langle \mathbf{Z}_1\mathbf{c}_n^{^{(\mathbf{z})}}, \mathbf{W}_1\mathbf{c}_l \rangle }\right) \qquad (3) \end{eqnarray*}
where
* $\varphi_1(\cdot)$ is a non-linear activation function.
* $\mathbf{Z_0} \in \mathbb{R}^{r_0 \times N}$
* $\mathbf{W_0} \in \mathbb{R}^{r_1 \times r_0}:\mbox{ random value matrix} \qquad \color{darkgray} \mbox{note that }  \mathbf{Z}_1 = \varphi_1(\mathbf{W_0}\mathbf{Z}_0) \in \mathbb{R}^{r_1 \times N}$
* $\mathbf{W_1} \in \mathbb{R}^{r_1 \times L}$
* $r_1 \ll r_0 $



````

````{tabbed} Recall: Softmax class .
```{literalinclude} ../../../hosgdSims/HoSGDlabSGD.py
:language: python
:pyobject: hosgdFunSoftMax
```

````


````{tabbed} Activation functions

~~~{tabbed} ReLU
$ \begin{array}{llcl}
\bullet & \phi(u) & = & \max \{ u,\, 0 \} \\
\bullet & \frac{\partial \phi(u)}{\partial u} & = &  \psi(u) = 1.*(u>0)
\end{array}
$


<details>
  <summary><span style="font-size:1.0em;">Python code</span></summary>

```python
import numpy as np

def afReLu(u, flag):

    if flag is False:
       return np.maximum(u,0)
    else:
       return 1.*(u>0)
```
</details>

~~~


~~~{tabbed} ELU

$ \begin{array}{llcl}
\bullet & \phi(u,\tau) & = & \max \{ u,\, 0 \} + \tau\cdot\min\{ \mbox{e}^{u}-1, 0 \} \\
\bullet & \frac{\partial \phi(u)}{\partial u} & = &  \psi(u) = 1.*(u>0) + \tau.*(u<0)*\mbox{e}^{u}
\end{array}
$


<details>
  <summary><span style="font-size:1.0em;">Python code</span></summary>

```python
import numpy as np

def afELU(u, flag, par1=None):

    if par1 is None:
       par1 = 0.5

    if flag is False:
       return np.maximum(u,0) + par1*np.min( np.exp(u)-1, 0 )
    else:
       return 1.*(u>0) + par1*(u<0)*(np.exp(u))
```
</details>

~~~

````

<details><summary><strong>Proposed solution</strong></summary>

<div style="line-height:50%;"> <br> </div>

Among other pausible solutions, here a very simple but not scalable (hidden layers point of view) is listed. A more elaborate solution is the one used along with the [Backpropagation](./BP) simulation.


````{tabbed} Dataset transformation

* Highlighted lines point out the key idea behind this soution: transform the input dataset, using the <code class="language-python">HL class</code> to that end, and treat it as the original one.
* The <code class="language-python">hosgdFunELMSoftMax</code> class is identical to the <code class="language-python">hosgdFunSoftMax</code> saved for the part shown below.

```{literalinclude} ../../../hosgdSims/HoSGDlabAF.py
:language: python
:pyobject: hosgdFunELMSoftMax
:emphasize-lines: 9,12,21-23
:lines: 1-26

```

````

````{tabbed} HL class

```{literalinclude} ../../../hosgdSims/HoSGDlabAF.py
:language: python
:pyobject: hosgdFunHL

```
````

````{tabbed} Activation function class

```{literalinclude} ../../../hosgdSims/HoSGDlabAF.py
:language: python
:pyobject: hosgdFunAF

```

````

<div style="line-height:50%;"> <br> </div>
<hr style="border:2px solid gray"> </hr>


<span style="font-size:1.5em;"><strong>Full solution</strong></span>


<details>
  <summary><span style="font-size:1.25em;">HoSGDlabAF.py</span></summary>

<div style="line-height:50%;"> <br> </div>

```{literalinclude} ../../../hosgdSims/HoSGDlabAF.py
:language: python
:lines: 3-26
```

</details>


<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">Routines to execute example and plot results</span></summary>

<div style="line-height:50%;"> <br> </div>

```{literalinclude} ../../../hosgdSims/HoSGDlabAF.py
:language: python
:lines: 273-375
```
```{literalinclude} ../../../hosgdSims/HoSGDlabSGDVariants.py
:language: python
:lines: 277-306
```

</details>

<div style="line-height:50%;"> <br> </div>

<details>
  <summary><span style="font-size:1.25em;">Simple Simulation (CIFAR-10)</span></summary>

```python

import HoSGDlabAF  as HAF

sol = HAF.exMultiClass(40, 128, 0.025, dataset='cifar10',verbose=-1)

```
![Sim](./elm-ex1a.png "elm")
![Sim](./elm-ex1b.png "elm")

</details>



</details>

<div style="line-height:50%;"> <br> </div>


