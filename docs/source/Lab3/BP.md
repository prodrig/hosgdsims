
<!-- .dDown[open] > summary {
  box-shadow: 3px 3px 4px black;
  cursor: pointer;
  list-style: none;}

  DarkOliveGreen
  DarkRed
  #283747;  similar (but darker) to
  DarkSlateGray;
-->


<style type="text/css">
<!--
 .tab1 { margin-left: 1em;}
 .tab2 { margin-left: 2em;}
 .dDown[open] > summary {
  color: DarkSlateGray;}
 .dDown > summary {
  color: DarkSlateGray;}
-->
</style>


(theoryBP)=
# Backpropagation

Considering a cost functional with the same characteristics as the one used for the {hoverxref}`SGD<theorySGD>` case, then
````{tabbed} Problem
* Modify the SGD routine so as to integrate it along with the backpropagation algorithm
* Test such modification on the classification problem solved by an architecture that includes one hidden layer
````

````{tabbed} Architecture & gradients

As detailed in the {hoverxref}`ELM simulation<theoryDetail>` the Softmax function along with a hidden layer can be summarized by

\begin{eqnarray*} F(\mathbf{W}_1, \mathbf{Z}_1) & = & \frac{1}{N}\sum_{n=0}^N \langle \mathbf{Z}_1\mathbf{c}_n^{^{(\mathbf{z})}}, \mathbf{W}_1\mathbf{h}_n \rangle + \frac{1}{N} \sum_{n=0}^N \log\left( \sum_{l=0}^{L-1}
                      \mbox{e}^{ -\langle \mathbf{Z}_1\mathbf{c}_n^{^{(\mathbf{z})}}, \mathbf{W}_1\mathbf{c}_l \rangle }\right) \qquad (1) \end{eqnarray*}
where
* $\mathbf{Z}_0 = \mathbf{X} \in \mathbb{R}^{r_0 \times N}$ represents the input dataset which has $L$ classes.
* $\mathbf{Z}_1 = \varphi_1(\mathbf{W_0}\mathbf{Z}_0)  \in \mathbb{R}^{r_1 \times N}$ represents a dense hidden layer.
* $\mathbf{W}_0 \in \mathbb{R}^{r_1 \times r_0}$: hidden layer's weights.
* $\varphi_1(\cdot)$ is a non-linear activation function.
* $\mathbf{W}_1 \in \mathbb{R}^{r_1 \times L}$: softmax's layer weights.
* Typical case $r_1 \ll r_0 $ (at least $r_1 < r_0 $).

and the vectors
* $\mathbf{c}_n^{^{(\mathbf{z})}}$ represents a canonical vector of length $\mathbf{Z}.\mbox{shape}[1]$
* $\mathbf{c}_l$ represents a canonical vector of length L
* $\mathbf{h}_n \in \mathbb{R}^L$ is the one-hot representation of $n^{th}$ training element's class.

are used to extract a particular column of a given matrix.

Using this notation, then the backpropagation algorithm applied to this case (for a full explanation see [Lecture 3b's Introduction & BP: softmax+HL sections](https://drive.google.com/file/d/1Kt1wPgYzIvMYtM19ft-XrSqrZ-Td43jb/view?usp=sharing))
 may be summarized by

$$\begin{array}{rcll}
    \nabla_{\mathbf{W}_1} F(\mathbf{W_1}, \mathbf{Z}_1) & = & \frac{1}{N}\mathbf{Z_1}\mathbf{H}^T - \frac{1}{N}\mathbf{Z}_1 \mbox{softmax}(-\mathbf{Z}_1^T \mathbf{W_1}, \mbox{axis=1}) & \quad(2.1) \\
    \nabla_{\mathbf{W_0}} \varphi_1(\mathbf{W_0}\mathbf{Z}_0) & = & \psi(\mathbf{W_0}\mathbf{Z}_0) \mathbf{Z}_0^T & \quad(2.2) \\
    \nabla_{\mathbf{Z_1}} F(\mathbf{W_1}, \mathbf{Z}_1) & = &  \frac{1}{N}\mathbf{W_1}\mathbf{H} - \frac{1}{N}\mathbf{W}_1 \mbox{softmax}^T(-\mathbf{Z}_1^T \mathbf{W_1}, \mbox{axis=1}) & \quad(2.3) \\
    & & & \\
    \nabla_{\mathbf{W_0}} F(\mathbf{W_1}, \mathbf{Z}_1) & = &  \left( \psi(\mathbf{W_0}\mathbf{Z}_0) \odot \nabla_{\mathbf{Z_1}} F(\mathbf{W_1}, \mathbf{Z}_1) \right) \mathbf{Z}_0^T & \quad(2.4)
\end{array}$$
where (2.1)-(2.3) are part of the forward pass, and (2.4) is the backward pass; furthermore, it is relevant to highlight that
* $\mathbf{H} \in \mathbb{R}^{L \times N}$ represents the one-hot matrix associated with the training dataset.
* $\mbox{softmax}(\mathbf{V}, \mbox{axis=1}) = \mathbf{S}$, where:
  * $\mathbf{V}, \mathbf{S} \in  \mathbb{R}^{\kappa \times \eta}$
  * $\mathbf{S}[k,:] = \frac{\mbox{e}^{\mathbf{V}[k,:]}}{\sum_{n=1}^\eta \mbox{e}^{\mathbf{V}[k,n]} }$

````


<details><summary><strong>Proposed solution</strong></summary>

<div style="line-height:50%;"> <br> </div>

Among other pausible solutions, the proposed one is detailed below


* <details><summary><strong>Differences between SGD and SGD+BP</strong></summary>

  Each of the tabs below highlights the key difference between the original SGD routine
  and the one that includes the backpropagation and trains a hidden layer.
  ~~~{tabbed} Weights initialization

  <table>
  <tr>
  <th>SGD</th>
  <th>SGD+BP</th>
  </tr>
  <tr>
  <td>

  ```{literalinclude} ../../../hosgdSims/HoSGDlabSGD.py
  :language: python
  :pyobject: hosgdSGD
  :emphasize-lines: 3
  ```

  </td>
  <td>

  ```{literalinclude} ../../../hosgdSims/HoSGDlabHL.py
  :language: python
  :pyobject: hosgdSGDHLSover
  :emphasize-lines: 3-7
  ```

  </td>
  </tr>
  </table>


  ~~~
  ~~~{tabbed} Initialization of SGD sub-routine

  <table>
  <tr>
  <th>SGD</th>
  <th>SGD+BP</th>
  </tr>
  <tr>
  <td>

  ```{literalinclude} ../../../hosgdSims/HoSGDlabSGD.py
  :language: python
  :pyobject: hosgdSGD
  ```

  </td>
  <td>

  ```{literalinclude} ../../../hosgdSims/HoSGDlabHL.py
  :language: python
  :pyobject: hosgdSGDHLSover
  :emphasize-lines: 16-17
  ```

  </td>
  </tr>
  </table>

  ~~~
  ~~~{tabbed} Apply forward operator

  <table>
  <tr>
  <th>SGD</th>
  <th>SGD+BP</th>
  </tr>
  <tr>
  <td>

  ```{literalinclude} ../../../hosgdSims/HoSGDlabSGD.py
  :language: python
  :pyobject: hosgdSGD
  ```

  </td>
  <td>

  ```{literalinclude} ../../../hosgdSims/HoSGDlabHL.py
  :language: python
  :pyobject: hosgdSGDHLSover
  :emphasize-lines: 40-44
  ```

  </td>
  </tr>
  </table>

  ~~~
  ~~~{tabbed} Update weights

  <table>
  <tr>
  <th>SGD</th>
  <th>SGD+BP</th>
  </tr>
  <tr>
  <td>

  ```{literalinclude} ../../../hosgdSims/HoSGDlabSGD.py
  :language: python
  :pyobject: hosgdSGD
  :emphasize-lines: 32-39
  ```

  </td>
  <td>

  ```{literalinclude} ../../../hosgdSims/HoSGDlabHL.py
  :language: python
  :pyobject: hosgdSGDHLSover
  :emphasize-lines: 46-56
  ```

  </td>
  </tr>
  </table>

  ~~~

  </details>


* <details><summary><strong>New structure for the OptProb class</strong></summary>

  ```{literalinclude} ../../../hosgdSims/HoSGDlabHL.py
  :language: python
  :pyobject: hosgdHLOptProb
  ```

  </details>


* <details><summary><strong>Layers (Softmax and dense) routines that are BP compatible</strong></summary>

  ~~~{tabbed} Softmax

  ```{literalinclude} ../../../hosgdSims/HoSGDlabHL.py
  :language: python
  :pyobject: hosgdHLFunSoftMax
  ~~~
  ~~~{tabbed} Dense

  ```{literalinclude} ../../../hosgdSims/HoSGDlabHL.py
  :language: python
  :pyobject: hosgdHLFunDense
  ```

  ~~~

  </details>


<div style="line-height:50%;"> <br> </div>
<hr style="border:2px solid gray"> </hr>


<span style="font-size:1.5em;"><strong>Full solution</strong></span>

<details>
  <summary><span style="font-size:1.25em;">Routines to execute example and plot results</span></summary>

<div style="line-height:50%;"> <br> </div>

```{literalinclude} ../../../hosgdSims/HoSGDlabHL.py
:language: python
:pyobject: exHLMultiClass
```

</details>

<div style="line-height:50%;"> <br> </div>


<details>
  <summary><span style="font-size:1.25em;">Simple Simulation (CIFAR-10)</span></summary>

```python

import numpy as np
import HoSGDdefs as hosgdDef
import HoSGDlabHL  as HL

sol = HL.exHLMultiClass(50, 128, np.array([0.001,0.00025]), dataset='cifar10', verbose=10, actFun=hosgdDef.actFun.ReLU)

```

![Sim](./bp-ex1a.png "Backpropagation")
![Sim](./bp-ex1b.png "Backpropagation")

</details>

<!-- Last -->
</details>

<div style="line-height:50%;"> <br> </div>


