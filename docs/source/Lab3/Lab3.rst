
Lecture 3
=========


.. collapse:: Theory

  Lecture slides:

  * Hidden layers, activation function, ELM: `Here <https://drive.google.com/file/d/1Dfk2xGBoPvWsx4rJwX9XMHZXq185cWzx/view?usp=sharing>`_
  * Backpropagation: `Here <https://drive.google.com/file/d/1Kt1wPgYzIvMYtM19ft-XrSqrZ-Td43jb/view?usp=sharing>`_


.. raw:: html

    <hr style="border:2px solid gray"> </hr>


These hands-on exercises focus on

* Highlighting having a linear hidden layer versus a non-linear one,

* Evaluating the impact of using one, random value, hidden layer (ie. ELM) along with an activation function (e.g. ReLU, ELU, etc.) for the MNIST and CIFAR-10 datasets.

* Integrating the Backpropagation (BP) algorithm to the SGD to fully train a network with one hidden layer for the multiclass classification problem (CIFAR-10 dataset).



.. toctree::
   :maxdepth: 1
   :caption: Simulations

   ELM.md
   BP.md
