# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))



# -- General configuration ---------------------------------------------------

import os
import sys

##sys.path.insert(0, os.path.abspath('../../'))
#cwd = os.path.dirname(os.path.abspath(__file__))
#sys.path.insert(0, os.path.abspath(os.path.join(cwd, '../../')))


# -- Project information -----------------------------------------------------

project = 'hosgdSims'
copyright = '2023, Paul Rodriguez'
author = 'Paul Rodriguez'

# --

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.

extensions = [
'sphinx.ext.autodoc',
'sphinx.ext.mathjax',
'sphinx.ext.napoleon',
'sphinx.ext.viewcode',
"sphinxcontrib.proof",
"sphinxcontrib.bibtex",
'matplotlib.sphinxext.plot_directive',
'sphinx.ext.intersphinx',
'sphinx_toolbox.collapse',
'sphinx_rtd_theme',
'myst_nb',
'nbsphinx',
'nbsphinx_link',
'sphinx_panels',
'sphinxcontrib.tikz',
'sphinx_inline_tabs',
'hoverxref.extension',
]


mathjax_config = {
    "TeX": {
        "Macros": {
            "argmin": [r"\mathop{\mathrm{argmin}}"],
            "sign": [r"\mathop{\mathrm{sign}}"],
            "prox": [r"\mathop{\mathrm{prox}}"],
        }
    }
}


myst_update_mathjax = False

myst_enable_extensions = [
    "substitution",
]


# Hoverxref Extension
hoverxref_auto_ref = True
hoverxref_api_host = 'http://community.dev.readthedocs.io'

hoverxref_role_types = {
    "hoverxref": "tooltip",
    "ref": "tooltip",  # for hoverxref_auto_ref config
}


# Other
autodoc_member_order = "bysource"

bibtex_bibfiles = ['biblioSGD.bib']

intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'numpy': ('https://numpy.org/doc/stable/', None),
    'scipy': ('https://docs.scipy.org/doc/scipy/reference/', None),
}


suppress_warnings = [
    'ref.citation',  # Many duplicated citations in numpy/scipy docstrings.
    'ref.footnote',  # Many unreferenced footnotes in numpy/scipy docstrings
]



source_suffix = ['.rst', '.ipynb', '.md']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# Panels
panels_add_bootstrap_css = True

panels_css_variables = {
    "tabs-color-label-active": "hsla(231, 99%, 66%, 1)",
    "tabs-color-label-inactive": "rgba(178, 206, 245, 0.62)",
    "tabs-color-overline": "rgb(207, 236, 238)",
    "tabs-color-underline": "rgb(207, 236, 238)",
    "tabs-size-label": "1rem",
}


# -- Options specific to sphinxcontrib-tikz -------------------------------

latex_elements = {
'preamble': r'''
\usepackage{tikz}
\usetikzlibrary{arrows.meta}
\usepackage{hf-tikz}
\usepackage{tikz-3dplot}
'''
}

# Use default suite
tikz_proc_suite = "GhostScript"


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#

#html_theme = 'alabaster'
#html_theme = 'pydata_sphinx_theme'

html_theme = "sphinx_rtd_theme"


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


# ===============

def setup(app):
    app.add_css_file('css/customSGD.css')

# Add `code-include` so that the code-include directives used in this documentation work
extensions += [
    "code_include.extension",
]




# -- Jupyter ----------------------------------------------
nb_execute_notebooks = "never"
nb_execution_allow_errors = False
nb_execution_fail_on_error = True

# Notebook timeout
nb_execution_timeout = 200


# -- Myst ----------------------------------------------
myst_enable_extensions = [
    "amsmath",
    "colon_fence",
    "deflist",
    "dollarmath",
    "html_image",
    "substitution",
    "replacements",
]
myst_url_schemes = ("http", "https", "mailto")




# ===============
