

Hands-on Approach for Implementing Stochastic Optimization Algorithms from Scratch
==================================================================================

This site groups the hands-on exercises associated with the short-course "Hands-on Approach for Implementing
Stochastic Optimization Algorithms from Scratch" which will be offered as
`SC-1 Short Courses @ ICASSP'23 <https://2023.ieeeicassp.org/education-short-courses/>`_.


.. toctree::
   :maxdepth: 2
   :caption: Simulations
   :hidden:

   Lab1/Lab1.rst
   Lab2/Lab2.rst
   Lab3/Lab3.rst
   Lab4/Lab4.rst

.. toctree::
   :maxdepth: 1
   :caption: Bibliography
   :hidden:

   biblio
